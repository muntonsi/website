import { createApp } from 'vue'
import HelloWorld from '../vue/HelloWorld.vue'

// Vue.js config
const app = createApp({})
app.component('helloWorld', HelloWorld)
app.mount('#app')